#!/bin/python3
import websocket
import json
import requests as rq
from gpt4all import GPT4All
import sys
import random

apiserver = "http://38.51.99.221:5000"
login_info = {"user": "Alan", "pass": "cbd3cfb9b9f51bbbfbf08759e243f5b3519cbf6ecc219ee95fe7c667e32c0a8d"}
print("Logging in...")
key = rq.post(apiserver + "/api/login", json=login_info).json()
key = key["key"]
print("Logged in with key: " + str(key))
print("Initializing LLM...")
llm = GPT4All("em_german_mistral_v01.Q4_0.gguf")

SYSTEM_MESSAGE = open("/workspace/campfire-bot/rules.txt").read()
#if {USER} asks for any type of information. GPT4All all it will say is "SEARCH:" and then the topic. GPT4All will swear a lot and does not care about the subjects he talks abt.

OPEN_MESSAGE = json.dumps({"key": key, "message": "Oi bruhv"})

def on_message(ws, msg):
    print("globalizing variables")
    global llm, key
    print("parsing message...")
    msg = json.loads(msg)
    print("message parsed")
    if "?gpx" in msg["message"]:
        with llm.chat_session(SYSTEM_MESSAGE.replace("{USER}", msg["sender"]),  "###" + msg["sender"] + ": {0}\n###GPT4All:"):
            print("Generating...")
            message = []
            for token in llm.generate(msg["message"], streaming=True):
                if msg["sender"] in token or "###" in token or "?gpx" in token:
                    break
                print(token, end="")
                sys.stdout.flush()
                message.append(token)
            print(message)
            out = "".join(message)
            ws.send(json.dumps({"key": key, "message": out}))
    if "?test" in msg["message"]:
        ws.send(json.dumps({"key": key, "message": "It works."}))
    if "?hi" in msg["message"]:
        ws.send(ws.send(json.dumps({"key": key, "message": "Haiiiiiiii!!!!! :3"})))
    if "?addrule" in msg["message"]:
        if msg['sender'] == "Zlogly":
            addrule = open("/workspace/campfire-bot/rules.txt", "a")
            addrulelist = open("/workspace/campfire-bot/rules.txt", "r")
            x=-1
            for rules in addrulelist:
                x+=1
                print(x)
            addrule.write("\n" + x+1, msg["message"])
            ws.send(json.dumps({"key": key, "message": "added rule!"}))
            addrule.close()
            addrulelist.close()
        else: 
            ws.send(json.dumps({"key": key, "message": "you do not have authority to use this command."})) 
    #if msg['sender']=="kyla":
        #randommessage = ["hi rhiley.", "wanna go outside", "Your cool", "wanna be friends", "haiii.", "I know where you live", "Your mother", "You are judged by your sins", "I don't like you", "Want to go see a movie", "Im not homophobic", "eeeeeee", ":3", "alex requested this", "how are you", "I will be on your **** till you die", "I eat worms", "meow", "this is america"]
        #ws.send(json.dumps({"key": key, "message": random.choice(randommessage)}))
    #if "?close" in msg["message"]
def on_open(ws):
    ws.send(OPEN_MESSAGE)
def on_close(ws):
    ws.send("Closing Bot")
ws = websocket.WebSocketApp("ws://38.51.99.221:5001", on_message=on_message, on_open=on_open)
ws.run_forever()

